package com.architecture.di.koin
import com.architecture.model.repo.HomeRepository
import org.koin.dsl.module



val repoModule = module {

    /**Provide HomeRepository class Singleton object
     * you can use it any KoinComponent class  below is declaration
     *  private val repo: HomeRepository by inject() */

    single { HomeRepository(get()) }

}