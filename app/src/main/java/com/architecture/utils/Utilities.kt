package com.architecture.utils

import android.content.Context
import android.widget.Toast


object Utilities {
    private var toast: Toast? = null
    fun showToast(context: Context?, msg: String) {
        if (toast != null)
            toast!!.cancel()
        if (context != null) {
            toast = Toast.makeText(context, msg, Toast.LENGTH_LONG)
            toast!!.show()
        }

    }
}