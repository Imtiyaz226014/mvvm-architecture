package com.architecture.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class Connectivity {
    companion object {
        fun getNetworkInfo(context: Context): NetworkInfo? {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo
        }

        fun isConnected(context: Context?): Boolean {
            if (context == null)
                return false
            val info = getNetworkInfo(context)
            return info != null && info.isConnected
        }

    }
}