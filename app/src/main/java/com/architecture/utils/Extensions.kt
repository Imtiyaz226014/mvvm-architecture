package com.architecture.utils

import android.widget.ImageView
import com.architecture.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun tryThis(block: () -> Any?) {
    try {
        block()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun ImageView.loadImage(url: String?){
    if(url.isNullOrBlank()) return
    val options: RequestOptions = RequestOptions()
        .placeholder(R.drawable.ic_rain)
        .centerCrop()
    try {
        Glide.with(this).load(url).apply(options).into(this)
    }catch (e: Exception){
        e.printStackTrace()
    }
}