package com.architecture.view.base;


import androidx.databinding.ViewDataBinding;

public interface BaseInterFace {

     int getLayout();

     void initUI(ViewDataBinding binding);

}
