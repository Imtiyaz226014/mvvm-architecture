package com.architecture.view.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.architecture.databinding.RowImageBinding
import com.architecture.model.bean.responses.Photo
import com.architecture.utils.loadImage

class SearchAdapter(context: Context, private var imagesList: ArrayList<Photo>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val layoutInflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SearchViewHolder(RowImageBinding.inflate(layoutInflater, parent, false))
    }

    override fun getItemCount(): Int {
        return imagesList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SearchViewHolder).bind(imagesList[position])
    }


    /** View Holders */
    inner class SearchViewHolder(private val binding: RowImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Photo?) {
            if (item == null) return
            binding.searchIv.loadImage("http://farm${item.farm}.static.flickr.com/${item.server}/${item.id}_${item.secret}.jpg")
        }
    }
}