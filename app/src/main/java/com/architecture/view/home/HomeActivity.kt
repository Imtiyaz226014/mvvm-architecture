package com.architecture.view.home

import android.app.ProgressDialog
import android.text.Editable
import android.text.TextWatcher
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.architecture.R
import com.architecture.databinding.ActivityHomeBinding
import com.architecture.model.bean.responses.Photo
import com.architecture.model.bean.responses.SearchImageResponse
import com.architecture.model.remote.ApiResponse
import com.architecture.utils.Connectivity
import com.architecture.utils.Utilities
import com.architecture.utils.tryThis
import com.architecture.view.base.BaseActivity
import com.architecture.view.home.adapter.SearchAdapter
import com.architecture.view_model.HomeViewModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeActivity : BaseActivity(), TextWatcher {

    private lateinit var mBinding: ActivityHomeBinding
    private var adapter: SearchAdapter? = null
    private val mViewModel: HomeViewModel by viewModel()
    private lateinit var searchedImageList: List<Photo>
    private var currentPage = 1


    override fun getLayout(): Int {
        return R.layout.activity_home
    }

    override fun initUI(binding: ViewDataBinding?) {
        this.mBinding = binding as ActivityHomeBinding
        searchEt?.addTextChangedListener(this)
        searchedImageList = ArrayList()
        /*** observe live data of viewModel*/
        mViewModel.getSearchLiveData().observe(this, searchObserver)
    }


    private fun initRecyclerView() {
        if(!::searchedImageList.isInitialized) return

        tryThis {
            searchedImageList.let {
                if (adapter == null) {
                    mBinding.listDataRv.layoutManager = GridLayoutManager(this, 3)
                    adapter = SearchAdapter(
                        this,
                        searchedImageList as ArrayList<Photo>
                    )
                    mBinding.listDataRv.adapter = adapter
                    mBinding.listDataRv.addOnScrollListener(scrollListener)
                } else {
                    currentPage++
                    adapter?.notifyDataSetChanged()
                }
            }
        }
    }

    private val searchObserver: Observer<ApiResponse<SearchImageResponse>> by lazy {
        Observer<ApiResponse<SearchImageResponse>> {
            handleResponse(it)
        }
    }
    var scrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (progressDialog.isShowing) return
            val visibleItemCount: Int = mBinding.listDataRv.childCount ?: 0
            val totalItemCount: Int = mBinding.listDataRv.layoutManager?.itemCount ?: 0
            val pastVisibleItems: Int =
                (mBinding.listDataRv.layoutManager as? GridLayoutManager)?.findFirstVisibleItemPosition()
                    ?: 0
            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                makeSearchApiCall(searchEt?.text.toString())
            }
        }
    }

    private fun makeSearchApiCall(text: String) {
        if (maxPageForSearch != -1 && currentPage > maxPageForSearch) return

        if (Connectivity.isConnected(this)) {
            /*** request viewModel to get data ***/
            text.run { mViewModel.callSearchApi(this, currentPage.toString()) }
        } else {
            Utilities.showToast(this, resources.getString(R.string.no_network_error))

        }
    }

    private var maxPageForSearch = -1

    /* Response Handlers */
    private fun handleResponse(response: ApiResponse<SearchImageResponse>) {
        when (response.status) {
            ApiResponse.Status.LOADING -> {
                if (currentPage == 1)
                    progressDialog.show()
            }
            ApiResponse.Status.SUCCESS -> {
                progressDialog.dismiss()
                if (response.data != null && response.data.stat == "ok") {
                    maxPageForSearch = response.data.photos.pages
                    if (response.data.photos.photo != null) {
                        (searchedImageList as? ArrayList)?.addAll(response.data.photos.photo)
                    }
                    initRecyclerView()
                } else {
                    Utilities.showToast(this, getString(R.string.no_data_found))
                }
            }

            ApiResponse.Status.ERROR -> {
                progressDialog.dismiss()
                Utilities.showToast(this, response.errorMessage.toString())
            }
        }
    }

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage(getString(R.string.please_wait_msg))
            setCancelable(false)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    var queryTextChangedJob: Job? = null

    override fun afterTextChanged(s: Editable?) {
        if (!s.isNullOrBlank()) {
            currentPage=1
            maxPageForSearch=-1
            queryTextChangedJob?.cancel()
            queryTextChangedJob = GlobalScope.launch(Dispatchers.Main) {
                delay(800)
                makeSearchApiCall(s.toString())
            }
        }else{
            currentPage = 0
        }
    }

}