package com.architecture.view_model

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.architecture.MyApplication
import com.architecture.model.bean.responses.SearchImageResponse
import com.architecture.model.remote.ApiResponse
import com.architecture.model.repo.HomeRepository

class HomeViewModel constructor(app: MyApplication, private var homeRepository: HomeRepository) : AndroidViewModel(app){
    private var searchLiveData = MutableLiveData<ApiResponse<SearchImageResponse>>()

    fun callSearchApi(text: String, currentPage: String) {
        homeRepository.getSearchData(searchLiveData,text,currentPage)
    }
    fun getSearchLiveData(): MutableLiveData<ApiResponse<SearchImageResponse>> {
        return searchLiveData
    }
}