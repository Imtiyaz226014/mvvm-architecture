package com.architecture.model.repo

import androidx.lifecycle.MutableLiveData
import com.architecture.model.bean.responses.SearchImageResponse
import com.architecture.model.remote.ApiResponse
import com.architecture.model.remote.ApiServices
import com.architecture.model.remote.DataFetchCall
import kotlinx.coroutines.Deferred
import org.koin.core.KoinComponent
import retrofit2.Response


class HomeRepository constructor(
    private val apiServices: ApiServices
) : KoinComponent {

    fun getSearchData(
        responseObservable: MutableLiveData<ApiResponse<SearchImageResponse>>,
        text: String,
        currentPage: String
    ) {
        object : DataFetchCall<SearchImageResponse>(responseObservable) {

            override fun createCallAsync(): Deferred<Response<SearchImageResponse>> {
                return apiServices.searchImage(text,currentPage)
            }

            override fun saveResult(result: SearchImageResponse) {
            }
        }.execute()
    }


}