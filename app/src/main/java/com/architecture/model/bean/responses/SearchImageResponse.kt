package com.architecture.model.bean.responses

data class SearchImageResponse(
    val photos: Photos,
    val stat: String
)