package com.architecture.model.remote

import com.architecture.model.bean.responses.SearchImageResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {

    @GET(ApiConstant.SEARCH_IMAGE)
    fun searchImage(@Query("text") text: String, @Query("page") currentPage: String): Deferred<Response<SearchImageResponse>>
}