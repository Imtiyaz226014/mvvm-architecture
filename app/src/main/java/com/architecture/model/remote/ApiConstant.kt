package com.architecture.model.remote

class ApiConstant {

    companion object {
        /*********API BASE URL************/
        const val BASE_URL ="https://api.flickr.com/services/rest/"
        const val API_TIME_OUT: Long = 6000
        const val API_KEY="3e7cc266ae2b0e0d78e279ce8e361736"
        const val SEARCH_IMAGE = "?method=flickr.photos.search&api_key=$API_KEY&format=json&nojsoncallback=1"
    }

}